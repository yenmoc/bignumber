﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System.Numerics;
using UnityEngine;
using UnityEngine.Profiling;


public class Brenchmark : MonoBehaviour
{
    private const int COUNT = 50000;

    private void Start()
    {
        //BrenchMarkAdd();
        //BrenchMarkAddBigNumber();
        BrenchMarkAddBigNumber2();
        //BrenchMarkAddBigNumber4();
    }

    private void BrenchMarkAdd()
    {
        Profiler.BeginSample("start add");
        const int a = 1000000000;
        const int b = 999999999;
        for (int i = 0; i < COUNT; i++)
        {
            var result = a + b;
        }

        Profiler.EndSample();
        Debug.Log(a + b);
    }

    private void BrenchMarkAddBigNumber()
    {
        Profiler.BeginSample("start add bignumber");

        UnityModule.BigNumbers.BigNumber a = 1000000000000000000;
        UnityModule.BigNumbers.BigNumber b = 4500000000000000000;
        // for (int i = 0; i < COUNT; i++)
        // {
        //     var result = a + b;
        // }

        Profiler.EndSample();
        Debug.Log((a + b).GetAlphabetString());
    }

    private void BrenchMarkAddBigNumber2()
    {
        Profiler.BeginSample("start add 2");

        BigNumbers.BigNumber a = "12345678";
        //BigNumbers.BigNumber b = "10";
        //BigNumbers.BigNumber e = "5";

        // for (int i = 0; i < COUNT; i++)
        // {
        //     var result = a + b;
        // }

        // BigNumbers.BigNumber c = null;
        // BigNumbers.BigNumber d = 0;
        // Profiler.BeginSample("Equal");
        // var r = a.Equal(b);
        // Profiler.EndSample();
        // Profiler.BeginSample("Equals");
        // var t = a.Equals(b);
        // Profiler.EndSample();
        //
        // Profiler.BeginSample("Equal ==");
        // var result = a == b;
        // Profiler.EndSample();

        // Debug.Log("a vs b" + a.Equals(b));
        // Debug.Log("a vs b" + (a == b));
        //
        // Debug.Log("a vs e" + a.Equals(e));
        // Debug.Log("a vs e" + (a == e));
        //
        // Debug.Log("a vs c" + a.Equals(c));
        // Debug.Log("a vs c" + (a == c));
        //
        // Debug.Log("a vs d" + a.Equals(d));
        // Debug.Log("a vs d" + (a == d));
        //
        // Debug.Log("c vs d" + c.Equals(d));
        // Debug.Log("c vs d" + (c == d));


        // Debug.Log(a + b);
        // Debug.Log(a / b);
        // Debug.Log(a - b);
        // Debug.Log(a * b);
        //
        // Debug.Log(++a);
        // a--;
        // Debug.Log(a);
        // a -= 2;
        // Debug.Log(a);

        Profiler.EndSample();
        Debug.Log(a);
    }
    

    private void BrenchMarkAddBigNumber4()
    {
        Profiler.BeginSample("start add big integer");

        BigInteger a = 10;
        //BigInteger b = 4500000000000000000;
        // for (int i = 0; i < COUNT; i++)
        // {
        //     var result = a + b;
        // }

        Profiler.EndSample();
        //Debug.Log(a + b);
    }
}