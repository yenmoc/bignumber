/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;

namespace BigNumbers
{
    public class BigNumberMath
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <typeparam name="TBigNumberType"></typeparam>
        /// <returns></returns>
        public static TBigNumberType Abs<TBigNumberType>(TBigNumberType number) where TBigNumberType : IBigNumber
        {
            var result = (TBigNumberType) number.Clone();
            if (result.IsNegative)
                result.Negate();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstNumber"></param>
        /// <param name="secondNumber"></param>
        /// <typeparam name="TBigNumberType"></typeparam>
        /// <returns></returns>
        public static TBigNumberType Gcd<TBigNumberType>(TBigNumberType firstNumber, TBigNumberType secondNumber) where TBigNumberType : IBigNumber
        {
            var b = (TBigNumberType) secondNumber.Clone();
            var a = (TBigNumberType) firstNumber.Clone();
            while (!b.IsZero)
            {
                var temp = b;
                b = (TBigNumberType) a.Mod(b);
                a = temp;
            }

            return a;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseNumber"></param>
        /// <param name="exponent"></param>
        /// <typeparam name="TBigNumberType"></typeparam>
        /// <returns></returns>
        public static TBigNumberType Power<TBigNumberType>(TBigNumberType baseNumber, int exponent) where TBigNumberType : IBigNumber
        {
            if (exponent < 0)
                return (TBigNumberType) baseNumber.Construct("0");
            var result = (TBigNumberType) baseNumber.Construct("1");
            var tempBase = (TBigNumberType) baseNumber.Clone();
            switch (exponent)
            {
                case 0:
                    return result;
                case 1:
                    return tempBase;
            }

            while (exponent > 0)
            {
                var dig = exponent & 1;
                if (dig == 1)
                {
                    result = (TBigNumberType) result.Mul(tempBase);
                    exponent--;
                }
                else
                {
                    tempBase = (TBigNumberType) tempBase.Mul(tempBase);
                    exponent >>= 1;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <typeparam name="TBigNumberType"></typeparam>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static TBigNumberType SquareRoot<TBigNumberType>(TBigNumberType number) where TBigNumberType : IBigNumber
        {
            if (number.IsNegative)
                throw new Exception("Squere root of negative numbers is not supported");
            if (number.IsZero)
                return (TBigNumberType) number.Construct("0");
            if (number.IsOne)
                return (TBigNumberType) number.Construct("1");

            var two = (TBigNumberType) number.Construct("2");
            var temp = (TBigNumberType) number.Clone();
            var next = (TBigNumberType) (temp.Add(number.Div(temp))).Div(two);
            while (!temp.Equal(next))
            {
                if (temp.Smaller(next))
                    return temp;
                temp = next;
                next = (TBigNumberType) (temp.Add(number.Div(temp))).Div(two);
            }

            return temp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstNumber"></param>
        /// <param name="secondNumber"></param>
        /// <typeparam name="TBigNumberType"></typeparam>
        /// <returns></returns>
        public static TBigNumberType Hcf<TBigNumberType>(TBigNumberType firstNumber, TBigNumberType secondNumber) where TBigNumberType : IBigNumber
        {
            TBigNumberType l;
            var n = firstNumber;
            var m = secondNumber;
            do
            {
                l = (TBigNumberType) n.Mod(m);
                n = m;
                m = l;
            } while (!m.IsZero);

            return n;
        }

        /// <summary>
        /// Calculates the factorial of the given number
        /// </summary>
        /// <param name="number">The number to calculate<</param>
        /// <typeparam name="TBigNumberType">A variable that determins the return type</typeparam>
        /// <returns></returns>
        public static TBigNumberType Factorial<TBigNumberType>(int number) where TBigNumberType : IBigNumber, new()
        {
            var t = new TBigNumberType();
            var result = (TBigNumberType) t.Construct("1");
            TBigNumberType temp;
            while (number > 1)
            {
                temp = (TBigNumberType) t.Construct(number.ToString());
                result = (TBigNumberType) result.Mul(temp);
                number--;
            }

            return result;
        }
    }
}