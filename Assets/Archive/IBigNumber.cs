/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

namespace BigNumbers
{
    public interface IBigNumber
    {
        /// <summary>
        /// Returns true if number less than 0
        /// </summary>
        bool IsNegative { get; }

        /// <summary>
        /// Returns true if the number is 0
        /// </summary>
        bool IsZero { get; }

        /// <summary>
        /// Returns true if the number is 1 or -1
        /// </summary>
        bool IsOne { get; }

        /// <summary>
        /// Initializes the number from a valid string
        /// </summary>
        /// <param name="number">A string representing a number</param>
        void Initialize(string number);

        /// <summary>
        /// Multiplies the number by -1 without creating a new instance of the number
        /// </summary>
        void Negate();

        /// <summary>
        /// add
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        IBigNumber Add(IBigNumber second);

        /// <summary>
        /// sub
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        IBigNumber Sub(IBigNumber second);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        IBigNumber Mul(IBigNumber second);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        IBigNumber Div(IBigNumber second);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        IBigNumber Mod(IBigNumber second);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IBigNumber Clone();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        IBigNumber Construct(string number);

        /// <summary>
        /// 
        /// </summary>
        void Abs();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        bool Equal(IBigNumber second);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        bool Smaller(IBigNumber second);
    }

    public class BigNumberEqualityComparer<T> : System.Collections.Generic.IEqualityComparer<T> where T : IBigNumber
    {
        public bool Equals(T x, T y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;
            return x.Equal(y);
        }

        public int GetHashCode(T obj)
        {
            return obj.ToString().GetHashCode();
        }
    }

    public interface IIntegerBigNumber : IBigNumber
    {
    }
}