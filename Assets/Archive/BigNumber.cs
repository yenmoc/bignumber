﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;
using System.Collections.Generic;

namespace BigNumbers
{
    [Serializable]
    internal struct DigitsArray
    {
        internal static readonly uint AllBits; // = ~((System.UInt32)0);
        internal static readonly uint HiBitSet; // = 0x80000000;
        internal static readonly int DataSizeOf;
        internal static readonly int DataSizeBits;

        private uint[] _data;
        internal int DataUsed { get; set; }

        internal int Count => _data.Length;

        internal bool IsZero => DataUsed == 0 || DataUsed == 1 && _data[0] == 0;

        internal bool IsOne => DataUsed == 1 && _data[0] == 1;

        internal DigitsArray(int size, int used = 0) : this()
        {
            _data = new uint[size];
            DataUsed = used;
        }

        internal DigitsArray(uint[] copyFrom) : this()
        {
            _data = new uint[copyFrom.Length];
            DataUsed = 0;
            CopyFrom(copyFrom, 0, 0, copyFrom.Length);
            RemoveLeadingZeroes();
        }

        internal DigitsArray(DigitsArray copyFrom) : this()
        {
            _data = new uint[copyFrom.Count];
            DataUsed = copyFrom.DataUsed;
            Array.Copy(copyFrom._data, 0, _data, 0, copyFrom.Count);
        }

        static DigitsArray()
        {
            unchecked
            {
                AllBits = 4294967295U;
                DataSizeOf = sizeof(uint);
                DataSizeBits = sizeof(uint) * 8;
                HiBitSet = (uint) 1 << DataSizeBits - 1;
            }
        }

        internal void CopyFrom(uint[] source, int sourceOffset, int offset, int length)
        {
            if (source == _data)
                throw new ArgumentException("CopyFrom - source cannot be equal to m_data");
            Array.Copy(source, sourceOffset, _data, offset, length);
        }

        internal void CopyTo(uint[] array, int offset, int length)
        {
            if (array == _data)
                throw new ArgumentException("CopyFrom - source cannot be equal to m_data");

            Array.Copy(_data, 0, array, offset, length);
        }

        internal uint this[int index]
        {
            get
            {
                if (index < DataUsed && index >= 0)
                    return _data[index];
                return 0;
            }
            set => _data[index] = value;
        }

        internal void RemoveLeadingZeroes()
        {
            DataUsed = _data.Length;
            while (DataUsed > 1 && _data[DataUsed - 1] == 0)
            {
                --DataUsed;
            }

            if (DataUsed == 0)
            {
                DataUsed = 1;
            }
        }

        internal int ShiftRight(int shiftCount)
        {
            return ShiftRight(_data, shiftCount);
        }

        internal static int ShiftRight(uint[] buffer, int shiftCount)
        {
            var bufLen = buffer.Length;

            while (bufLen > 1 && buffer[bufLen - 1] == 0)
            {
                bufLen--;
            }

            var fullShift = shiftCount / DataSizeBits;
            var partShift = shiftCount % DataSizeBits;
            var target = 0;
            var source = fullShift;
            uint mask = 1;
            mask <<= partShift;
            mask--;

            uint top;
            var shiftComplement = DataSizeBits - partShift;
            while (source < bufLen)
            {
                if (target > 0)
                {
                    top = buffer[source] & mask;
                    top <<= shiftComplement;
                    buffer[target - 1] = buffer[target - 1] | top;
                }

                buffer[target] = buffer[source] >> partShift;
                target++;
                source++;
            }

            while (bufLen > 1 && buffer[bufLen - 1] == 0)
            {
                bufLen--;
            }

            return bufLen;
        }

        internal int ShiftLeft(int shiftCount)
        {
            return ShiftLeft(_data, shiftCount);
        }

        internal static int ShiftLeft(uint[] buffer, int shiftCount)
        {
            var shiftAmount = DataSizeBits;
            var bufLen = buffer.Length;

            while (bufLen > 1 && buffer[bufLen - 1] == 0)
            {
                bufLen--;
            }

            for (var count = shiftCount; count > 0; count -= shiftAmount)
            {
                if (count < shiftAmount)
                {
                    shiftAmount = count;
                }

                ulong carry = 0;
                for (var i = 0; i < bufLen; i++)
                {
                    var val = (ulong) buffer[i] << shiftAmount;
                    val |= carry;

                    buffer[i] = (uint) (val & AllBits);
                    carry = val >> DataSizeBits;
                }

                if (carry != 0)
                {
                    if (bufLen + 1 <= buffer.Length)
                    {
                        buffer[bufLen] = (uint) carry;
                        bufLen++;
                        carry = 0;
                    }
                    else
                    {
                        throw new OverflowException();
                    }
                }
            }

            return bufLen;
        }

        internal int ShiftLeftWithoutOverflow(int shiftCount)
        {
            var temporary = new List<uint>(_data);
            var shiftAmount = DataSizeBits;

            for (var count = shiftCount; count > 0; count -= shiftAmount)
            {
                if (count < shiftAmount)
                {
                    shiftAmount = count;
                }

                ulong carry = 0;
                for (var i = 0; i < temporary.Count; i++)
                {
                    var val = (ulong) temporary[i] << shiftAmount;
                    val |= carry;

                    temporary[i] = (uint) (val & AllBits);
                    carry = val >> DataSizeBits;
                }

                if (carry != 0)
                {
                    temporary.Add(0);
                    temporary[temporary.Count - 1] = (uint) carry;
                }
            }

            _data = new uint[temporary.Count];
            temporary.CopyTo(_data);
            return _data.Length;
        }

        internal void Serialize(System.IO.BinaryWriter writer)
        {
            foreach (var digit in _data)
            {
                writer.Write(digit);
            }
        }

        internal void Deserialize(System.IO.BinaryReader reader, int count)
        {
            _data = new uint[count];
            DataUsed = count;
            for (var i = 0; i < count; i++)
                _data[i] = reader.ReadUInt32();
            RemoveLeadingZeroes();
        }
    }

    [Serializable]
    public struct BigNumber : IBigNumber, IComparable<BigNumber>, IEquatable<BigNumber>
    {
        #region -- os --------------------

        internal DigitsArray digits;
        internal bool negative;

        #endregion------------------------------------------------------------\\
        
        /// <summary>
        /// A bool value that is true when the BigNumber is negative (less than zero).
        /// </summary>
        public bool IsNegative
        {
            get => negative;
            internal set
            {
                if (!IsZero)
                {
                    negative = value;
                }
            }
        }

        /// <summary>
        /// A bool value that is true when the BigNumber is exactly zero.
        /// </summary>
        public bool IsZero => digits.IsZero;

        /// <summary>
        /// A bool value that is true when the BigNumber is exactly one.
        /// </summary>
        public bool IsOne => digits.IsOne;

        internal BigNumber(BigNumber value)
        {
            digits = value.digits;
            negative = value.IsNegative;
        }

        /// <summary>Initializes a new instance of the <see cref="T:BigNumbers.BigNumber" /> structure using a 64-bit signed integer value.</summary>
        /// <param name="value">A 64-bit signed integer.</param>
        public BigNumber(long value)
        {
            negative = value < 0;
            value = Math.Abs(value);
            var size = sizeof(long) / DigitsArray.DataSizeOf;
            digits = new DigitsArray(size, size);
            var i = 0;
            while (value > 0)
            {
                digits[i] = (uint) (value & DigitsArray.AllBits);
                i++;
                value >>= DigitsArray.DataSizeBits;
            }

            digits.RemoveLeadingZeroes();
        }

        /// <summary>Initializes a new instance of the <see cref="T:BigNumbers.BigNumber" /> structure with an unsigned 64-bit integer value.</summary>
        /// <param name="value">An unsigned 64-bit integer.</param>
        public BigNumber(ulong value)
        {
            negative = false;
            var size = sizeof(ulong) / DigitsArray.DataSizeOf;
            digits = new DigitsArray(size, size);
            var i = 0;
            while (value > 0)
            {
                digits[i] = (uint) (value & DigitsArray.AllBits);
                i++;
                value >>= DigitsArray.DataSizeBits;
            }

            digits.RemoveLeadingZeroes();
        }

        /// <summary>
        /// Creates a BigNumber in base-10 from the parameter.
        /// </summary>
        /// <remarks>
        ///  The new BigNumber is negative if the <paramref name="value" /> has a leading - (minus).
        /// </remarks>
        /// <param name="value">A string</param>
        public BigNumber(string value)
        {
            negative = false;
            if (string.IsNullOrEmpty(value))
            {
                value = "0";
            }

            const int radix = 10;
            BigNumber multiplier = 1;
            BigNumber result = 0;
            value = value.ToUpper(System.Globalization.CultureInfo.CurrentCulture).Trim();

            var newValue = value[0] == '-' ? 1 : 0;

            for (var idx = value.Length - 1; idx >= newValue; idx--)
            {
                int d = value[idx];
                if (d >= '0' && d <= '9')
                {
                    d -= '0';
                }
                else if (d >= 'A' && d <= 'Z')
                {
                    d = d - 'A' + 10;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                if (d >= radix)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                result += (multiplier * d);
                multiplier *= radix;
            }

            if (value[0] == '-')
            {
                negative = true;
            }

            digits = result.digits;
        }

        /// <summary>
        /// Creates a BigNumber initialized from the byte array.
        /// </summary>
        /// <param name="array"></param>
        private BigNumber(byte[] array) : this(array, 0, array.Length)
        {
        }

        /// <summary>
        /// Creates a BigNumber initialized from the byte array ending at <paramref name="length" />.
        /// </summary>
        /// <param name="array">A byte array.</param>
        /// <param name="length">Int number of bytes to use.</param>
        private BigNumber(byte[] array, int length) : this(array, 0, length)
        {
        }

        /// <summary>
        /// Creates a BigNumber initialized from <paramref name="length" /> bytes starting at <paramref name="offset" />.
        /// </summary>
        /// <param name="array">A byte array.</param>
        /// <param name="offset">Int offset into the <paramref name="array" />.</param>
        /// <param name="length">Int number of bytes.</param>
        private BigNumber(byte[] array, int offset, int length)
        {
            negative = false;
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (offset > array.Length || length > array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(offset));
            }

            if (length > array.Length || offset + length > array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            var estSize = length / 4;
            var leftOver = length & 3;
            if (leftOver != 0)
            {
                ++estSize;
            }

            digits = new DigitsArray(estSize + 1); // alloc one extra since we can't init -'s from here.

            for (int i = offset + length - 1, j = 0; i - offset >= 3; i -= 4, j++)
            {
                digits[j] = (uint) ((array[i - 3] << 24) + (array[i - 2] << 16) + (array[i - 1] << 8) + array[i]);
                digits.DataUsed++;
            }

            uint accumulator = 0;
            for (var i = leftOver; i > 0; i--)
            {
                uint digit = array[offset + leftOver - i];
                digit = digit << ((i - 1) * 8);
                accumulator |= digit;
            }

            digits[digits.DataUsed] = accumulator;

            digits.RemoveLeadingZeroes();
        }

        /// <summary>
        /// Copy constructor, doesn't copy the digits parameter, assumes <code>this</code> owns the DigitsArray.
        /// </summary>
        /// <remarks>The <paramef name="digits" /> parameter is saved and reset.</remarks>
        /// <param name="digits"></param>
        private BigNumber(DigitsArray digits)
        {
            negative = false;
            digits.RemoveLeadingZeroes();
            this.digits = digits;
        }

        #region -- implicit --------------------

        /// <summary>
        /// Creates a BigNumber from a long.
        /// </summary>
        /// <param name="value">A long.</param>
        /// <returns>A BigNumber initialzed by <paramref name="value" />.</returns>
        public static implicit operator BigNumber(long value)
        {
            return new BigNumber(value);
        }

        /// <summary>
        /// Creates a BigNumber from a ulong.
        /// </summary>
        /// <param name="value">A ulong.</param>
        /// <returns>A BigNumber initialzed by <paramref name="value" />.</returns>
        public static implicit operator BigNumber(ulong value)
        {
            return new BigNumber(value);
        }

        /// <summary>
        /// Creates a BigNumber from a int.
        /// </summary>
        /// <param name="value">A int.</param>
        /// <returns>A BigNumber initialzed by <paramref name="value" />.</returns>
        public static implicit operator BigNumber(int value)
        {
            return new BigNumber(value);
        }

        /// <summary>
        /// Creates a BigNumber from a uint.
        /// </summary>
        /// <param name="value">A uint.</param>
        /// <returns>A BigNumber initialzed by <paramref name="value" />.</returns>
        public static implicit operator BigNumber(uint value)
        {
            return new BigNumber((ulong) value);
        }

        /// <summary>
        /// Creates a BigNumber from a string.
        /// </summary>
        /// <param name="value">A string.</param>
        /// <returns>A BigNumber initialzed by <paramref name="value" />.</returns>
        public static implicit operator BigNumber(string value)
        {
            return new BigNumber(value);
        }

        #endregion------------------------------------------------------------\\

        #region -- add & sub operator --------------------

        /// <summary>
        /// Adds two BigNumbers and returns a new BigNumber that represents the sum.
        /// </summary>
        /// <param name="leftSide">A BigNumber</param>
        /// <param name="rightSide">A BigNumber</param>
        /// <returns>The BigNumber result of adding <paramref name="leftSide" /> and <paramref name="rightSide" />.</returns>
        public static BigNumber operator +(BigNumber leftSide, BigNumber rightSide)
        {
            return Add(leftSide, rightSide);
        }

        /// <summary>
        /// Adds two BigNumbers and returns a new BigNumber that represents the sum.
        /// </summary>
        /// <param name="leftSide">A BigNumber</param>
        /// <param name="rightSide">A BigNumber</param>
        /// <returns>The BigNumber result of adding <paramref name="leftSide" /> and <paramref name="rightSide" />.</returns>
        private static BigNumber Add(BigNumber leftSide, BigNumber rightSide)
        {
            if (leftSide.IsZero)
                return new BigNumber(rightSide);
            if (rightSide.IsZero)
                return new BigNumber(leftSide);

            #region Handle negative numbers

            if (leftSide.IsNegative && rightSide.IsNegative)
            {
                rightSide.Negate();
                if (!leftSide.Equals(rightSide)) //In case we activated the function on this
                    leftSide.Negate();
                var innerResult = Add(leftSide, rightSide);
                rightSide.Negate();
                if (!leftSide.Equals(rightSide))
                    leftSide.Negate(); //In case we activated the function on this
                innerResult.Negate();
                return innerResult;
            }

            if (leftSide.IsNegative)
            {
                leftSide.Negate();
                var innerResult = Subtract(rightSide, leftSide);
                leftSide.Negate();
                return innerResult;
            }

            if (rightSide.IsNegative)
            {
                rightSide.Negate();
                var innerResult = Subtract(leftSide, rightSide);
                rightSide.Negate();
                return innerResult;
            }

            #endregion

            var size = Math.Max(leftSide.digits.DataUsed, rightSide.digits.DataUsed);
            var da = new DigitsArray(size + 1);

            long carry = 0;
            for (var i = 0; i < da.Count; i++)
            {
                var sum = (long) leftSide.digits[i] + rightSide.digits[i] + carry;
                carry = sum >> DigitsArray.DataSizeBits;
                da[i] = (uint) (sum & DigitsArray.AllBits);
            }

            return new BigNumber(da);
        }

        /// <summary>
        /// Increments the BigNumber operand by 1.
        /// </summary>
        /// <param name="leftSide">The BigNumber operand.</param>
        /// <returns>The value of <paramref name="leftSide" /> incremented by 1.</returns>
        public static BigNumber operator ++(BigNumber leftSide)
        {
            return Increment(leftSide);
        }

        /// <summary>
        /// Increments the BigNumber operand by 1.
        /// </summary>
        /// <param name="leftSide">The BigNumber operand.</param>
        /// <returns>The value of <paramref name="leftSide" /> incremented by 1.</returns>
        private static BigNumber Increment(BigNumber leftSide)
        {
            return leftSide + 1;
        }

        /// <summary>
        /// Substracts two BigNumbers and returns a new BigNumber that represents the sum.
        /// </summary>
        /// <param name="leftSide">A BigNumber</param>
        /// <param name="rightSide">A BigNumber</param>
        /// <returns>The BigNumber result of substracting <paramref name="leftSide" /> and <paramref name="rightSide" />.</returns>
        public static BigNumber operator -(BigNumber leftSide, BigNumber rightSide)
        {
            return Subtract(leftSide, rightSide);
        }

        /// <summary>
        /// Substracts two BigNumbers and returns a new BigNumber that represents the sum.
        /// </summary>
        /// <param name="leftSide">A BigNumber</param>
        /// <param name="rightSide">A BigNumber</param>
        /// <returns>The BigNumber result of substracting <paramref name="leftSide" /> and <paramref name="rightSide" />.</returns>
        private static BigNumber Subtract(BigNumber leftSide, BigNumber rightSide)
        {
            if (rightSide.IsZero)
                return new BigNumber(leftSide);
            if (leftSide.IsZero)
            {
                var result = new BigNumber(rightSide);
                result.Negate();
                return result;
            }

            #region Handle Negative Numbers

            if (leftSide.IsNegative || rightSide.IsNegative)
            {
                rightSide.Negate();
                var innerResult = Add(leftSide, rightSide);
                return innerResult;
            }

            #endregion

            //Subtract only when leftSide > rightSide
            if (leftSide.CompareTo(rightSide) < 0)
            {
                var innerResult = Subtract(rightSide, leftSide);
                innerResult.Negate();
                return innerResult;
            }

            var size = Math.Max(leftSide.digits.DataUsed, rightSide.digits.DataUsed) + 1;
            var da = new DigitsArray(size);

            long carry = 0;
            for (var i = 0; i < da.Count; i++)
            {
                var diff = leftSide.digits[i] - (long) rightSide.digits[i] - carry;
                da[i] = (uint) (diff & DigitsArray.AllBits);
                da.DataUsed++;
                carry = ((diff < 0) ? 1 : 0);
            }

            return new BigNumber(da);
        }

        /// <summary>
        /// Decrements the BigNumber operand by 1.
        /// </summary>
        /// <param name="leftSide">The BigNumber operand.</param>
        /// <returns>The value of the <paramref name="leftSide" /> decremented by 1.</returns>
        public static BigNumber operator --(BigNumber leftSide)
        {
            return Decrement(leftSide);
        }

        /// <summary>
        /// Decrements the BigNumber operand by 1.
        /// </summary>
        /// <param name="leftSide">The BigNumber operand.</param>
        /// <returns>The value of the <paramref name="leftSide" /> decremented by 1.</returns>
        private static BigNumber Decrement(BigNumber leftSide)
        {
            return (leftSide - 1);
        }

        #endregion------------------------------------------------------------\\

        #region -- negative operator --------------------

        /// <summary>
        /// Negates the BigNumber, that is, if the BigNumber is negative return a positive BigNumber, and if the
        /// BigNumber is negative return the postive.
        /// </summary>
        /// <param name="leftSide">A BigNumber operand.</param>
        /// <returns>The value of the this negated.</returns>
        public static BigNumber operator -(BigNumber leftSide)
        {
            var result = new BigNumber(leftSide);
            result.Negate();
            return result;
        }

        /// <summary>
        /// Negates the BigNumber, that is, if the BigNumber is negative return a positive BigNumber, and if the
        /// BigNumber is negative return the postive.
        /// </summary>
        /// <returns>The value of the this negated.</returns>
        public void Negate()
        {
            if (IsZero)
                negative = false;
            negative = !negative;
        }

        public void Abs()
        {
            negative = false;
        }

        #endregion------------------------------------------------------------\\

        #region -- mul & div & mod operator --------------------

        /// <summary>
        /// Multiply two BigNumbers returning the result.
        /// </summary>
        /// <remarks>
        /// See Knuth.
        /// </remarks>
        /// <param name="leftSide">A BigNumber.</param>
        /// <param name="rightSide">A BigNumber</param>
        /// <returns></returns>
        public static BigNumber operator *(BigNumber leftSide, BigNumber rightSide)
        {
            return Multiply(leftSide, rightSide);
        }

        /// <summary>
        /// Multiply two BigNumbers returning the result.
        /// </summary>
        /// <param name="leftSide">A BigNumber.</param>
        /// <param name="rightSide">A BigNumber</param>
        /// <returns></returns>
        private static BigNumber Multiply(BigNumber leftSide, BigNumber rightSide)
        {
            var leftSideNeg = leftSide.IsNegative;
            var rightSideNeg = rightSide.IsNegative;

            leftSide.Abs();
            rightSide.Abs();

            var da = new DigitsArray(leftSide.digits.DataUsed + rightSide.digits.DataUsed);
            da.DataUsed = da.Count;

            for (var i = 0; i < leftSide.digits.DataUsed; i++)
            {
                ulong carry = 0;
                for (int j = 0, k = i; j < rightSide.digits.DataUsed; j++, k++)
                {
                    var val = (ulong) leftSide.digits[i] * rightSide.digits[j] + da[k] + carry;

                    da[k] = (uint) (val & DigitsArray.AllBits);
                    carry = (val >> DigitsArray.DataSizeBits);
                }

                if (carry != 0)
                {
                    da[i + rightSide.digits.DataUsed] = (uint) carry;
                }
            }

            //da.ResetDataUsed();
            var result = new BigNumber(da);
            return leftSideNeg != rightSideNeg ? -result : result;
        }

        /// <summary>
        /// Divide a BigNumber by another BigNumber and returning the result.
        /// </summary>
        /// <param name="leftSide">A BigNumber divisor.</param>
        /// <param name="rightSide">A BigNumber dividend.</param>
        /// <returns>The BigNumber result.</returns>
        public static BigNumber operator /(BigNumber leftSide, BigNumber rightSide)
        {
            return Divide(leftSide, rightSide);
        }

        /// <summary>
        /// Divide a BigNumber by another BigNumber and returning the result.
        /// </summary>
        /// <param name="leftSide">A BigNumber divisor.</param>
        /// <param name="rightSide">A BigNumber dividend.</param>
        /// <returns>The BigNumber result.</returns>
        private static BigNumber Divide(BigNumber leftSide, BigNumber rightSide)
        {
            if (rightSide.IsZero)
            {
                throw new DivideByZeroException();
            }

            var divisorNeg = rightSide.IsNegative;
            var dividendNeg = leftSide.IsNegative;

            leftSide.Abs();
            rightSide.Abs();

            if (leftSide < rightSide)
            {
                return new BigNumber(0);
            }

            Divide(leftSide, rightSide, out var quotient, out _);
            return dividendNeg != divisorNeg ? -quotient : quotient;
        }

        private static void Divide(BigNumber leftSide, BigNumber rightSide, out BigNumber quotient, out BigNumber remainder)
        {
            if (leftSide.IsZero)
            {
                quotient = new BigNumber();
                remainder = new BigNumber();
                return;
            }

            if (rightSide.digits.DataUsed == 1)
            {
                SingleDivide(leftSide, rightSide, out quotient, out remainder);
            }
            else
            {
                MultiDivide(leftSide, rightSide, out quotient, out remainder);
            }
        }

        private static void MultiDivide(BigNumber leftSide, BigNumber rightSide, out BigNumber quotient, out BigNumber remainder)
        {
            var leftNegative = leftSide.IsNegative;
            if (rightSide.IsZero)
            {
                throw new DivideByZeroException();
            }

            var val = rightSide.digits[rightSide.digits.DataUsed - 1];
            var d = 0;
            for (var mask = DigitsArray.HiBitSet; mask != 0 && (val & mask) == 0; mask >>= 1)
            {
                d++;
            }

            var remainderLen = leftSide.digits.DataUsed + 1;
            var remainderDat = new uint[remainderLen];
            leftSide.digits.CopyTo(remainderDat, 0, leftSide.digits.DataUsed);

            DigitsArray.ShiftLeft(remainderDat, d);
            rightSide <<= d;

            ulong firstDivisor = rightSide.digits[rightSide.digits.DataUsed - 1];
            ulong secondDivisor = (rightSide.digits.DataUsed < 2 ? 0 : rightSide.digits[rightSide.digits.DataUsed - 2]);

            var divisorLen = rightSide.digits.DataUsed + 1;
            var dividendPart = new DigitsArray(divisorLen, divisorLen);
            var result = new uint[leftSide.digits.Count + 1];
            var resultPos = 0;

            var carryBit = (ulong) 0x1 << DigitsArray.DataSizeBits; // 0x100000000
            for (int j = remainderLen - rightSide.digits.DataUsed, pos = remainderLen - 1; j > 0; j--, pos--)
            {
                var dividend = ((ulong) remainderDat[pos] << DigitsArray.DataSizeBits) + remainderDat[pos - 1];
                var qHat = (dividend / firstDivisor);
                var rHat = (dividend % firstDivisor);

                while (pos >= 2)
                {
                    if (qHat == carryBit || (qHat * secondDivisor) > ((rHat << DigitsArray.DataSizeBits) + remainderDat[pos - 2]))
                    {
                        qHat--;
                        rHat += firstDivisor;
                        if (rHat < carryBit)
                        {
                            continue;
                        }
                    }

                    break;
                }

                for (var h = 0; h < divisorLen; h++)
                {
                    dividendPart[divisorLen - h - 1] = remainderDat[pos - h];
                }

                var dTemp = new BigNumber(dividendPart);
                var rTemp = rightSide * (long) qHat;
                while (rTemp > dTemp)
                {
                    qHat--;
                    rTemp -= rightSide;
                }

                rTemp = dTemp - rTemp;
                for (var h = 0; h < divisorLen; h++)
                {
                    remainderDat[pos - h] = rTemp.digits[rightSide.digits.DataUsed - h];
                }

                result[resultPos++] = (uint) qHat;
            }

            Array.Reverse(result, 0, resultPos);
            quotient = new BigNumber(new DigitsArray(result));

            var n = DigitsArray.ShiftRight(remainderDat, d);
            var rDa = new DigitsArray(n, n);
            rDa.CopyFrom(remainderDat, 0, 0, rDa.DataUsed);
            remainder = new BigNumber(rDa) {IsNegative = leftNegative};
        }

        private static void SingleDivide(BigNumber leftSide, BigNumber rightSide, out BigNumber quotient, out BigNumber remainder)
        {
            var leftNegative = leftSide.IsNegative;
            if (rightSide.IsZero)
            {
                throw new DivideByZeroException();
            }

            var remainderDigits = new DigitsArray(leftSide.digits);
            remainderDigits.RemoveLeadingZeroes();

            var pos = remainderDigits.DataUsed - 1;
            var divisor = (ulong) rightSide.digits[0];
            var dividend = (ulong) remainderDigits[pos];

            var result = new uint[leftSide.digits.Count];
            leftSide.digits.CopyTo(result, 0, result.Length);
            var resultPos = 0;

            if (dividend >= divisor)
            {
                result[resultPos++] = (uint) (dividend / divisor);
                remainderDigits[pos] = (uint) (dividend % divisor);
            }

            pos--;

            while (pos >= 0)
            {
                dividend = ((ulong) (remainderDigits[pos + 1]) << DigitsArray.DataSizeBits) + remainderDigits[pos];
                result[resultPos++] = (uint) (dividend / divisor);
                remainderDigits[pos + 1] = 0;
                remainderDigits[pos--] = (uint) (dividend % divisor);
            }

            remainder = new BigNumber(remainderDigits) {IsNegative = leftNegative};

            var quotientDigits = new DigitsArray(resultPos + 1, resultPos);
            var j = 0;
            for (var i = quotientDigits.DataUsed - 1; i >= 0; i--, j++)
            {
                quotientDigits[j] = result[i];
            }

            quotient = new BigNumber(quotientDigits);
        }

        /// <summary>
        /// Perform the modulus of a BigNumber with another BigNumber and return the result.
        /// </summary>
        /// <param name="leftSide">A BigNumber divisor.</param>
        /// <param name="rightSide">A BigNumber dividend.</param>
        /// <returns>The BigNumber result.</returns>
        public static BigNumber operator %(BigNumber leftSide, BigNumber rightSide)
        {
            return Modulus(leftSide, rightSide);
        }

        /// <summary>
        /// Perform the modulus of a BigNumber with another BigNumber and return the result.
        /// </summary>
        /// <param name="leftSide">A BigNumber divisor.</param>
        /// <param name="rightSide">A BigNumber dividend.</param>
        /// <returns>The BigNumber result.</returns>
        private static BigNumber Modulus(BigNumber leftSide, BigNumber rightSide)
        {
            var leftNegative = leftSide.IsNegative;

            if (rightSide.IsZero)
            {
                throw new DivideByZeroException();
            }

            var tempLeftSide = BigNumberMath.Abs(leftSide);
            var tempRightSide = BigNumberMath.Abs(rightSide);

            if (tempLeftSide < tempRightSide)
            {
                return new BigNumber(leftSide);
            }

            Divide(leftSide, rightSide, out _, out var remainder);
            remainder.IsNegative = leftNegative;
            return remainder;
        }

        #endregion------------------------------------------------------------\\

        #region -- bitwise operator --------------------

        public static BigNumber operator &(BigNumber leftSide, BigNumber rightSide)
        {
            var len = Math.Max(leftSide.digits.DataUsed, rightSide.digits.DataUsed);
            var da = new DigitsArray(len, len);
            for (var idx = 0; idx < len; idx++)
            {
                da[idx] = leftSide.digits[idx] & rightSide.digits[idx];
            }

            return new BigNumber(da);
        }

        private static BigNumber BitwiseAnd(BigNumber leftSide, BigNumber rightSide)
        {
            return leftSide & rightSide;
        }

        public static BigNumber operator |(BigNumber leftSide, BigNumber rightSide)
        {
            var len = Math.Max(leftSide.digits.DataUsed, rightSide.digits.DataUsed);
            var da = new DigitsArray(len, len);
            for (var idx = 0; idx < len; idx++)
            {
                da[idx] = leftSide.digits[idx] | rightSide.digits[idx];
            }

            return new BigNumber(da);
        }

        private static BigNumber BitwiseOr(BigNumber leftSide, BigNumber rightSide)
        {
            return leftSide | rightSide;
        }

        public static BigNumber operator ^(BigNumber leftSide, BigNumber rightSide)
        {
            var len = Math.Max(leftSide.digits.DataUsed, rightSide.digits.DataUsed);
            var da = new DigitsArray(len, len);
            for (var idx = 0; idx < len; idx++)
            {
                da[idx] = leftSide.digits[idx] ^ rightSide.digits[idx];
            }

            return new BigNumber(da);
        }

        private static BigNumber Xor(BigNumber leftSide, BigNumber rightSide)
        {
            return leftSide ^ rightSide;
        }

        public static BigNumber operator ~(BigNumber leftSide)
        {
            var da = new DigitsArray(leftSide.digits.Count);
            for (var idx = 0; idx < da.Count; idx++)
            {
                da[idx] = ~leftSide.digits[idx];
            }

            return new BigNumber(da);
        }

        private static BigNumber OnesComplement(BigNumber leftSide)
        {
            return ~leftSide;
        }

        #endregion------------------------------------------------------------\\

        #region -- left and right shift operator --------------------

        public static BigNumber operator <<(BigNumber leftSide, int shiftCount)
        {
            var da = new DigitsArray(leftSide.digits);
            da.DataUsed = da.ShiftLeftWithoutOverflow(shiftCount);

            return new BigNumber(da);
        }

        private static BigNumber LeftShift(BigNumber leftSide, int shiftCount)
        {
            return leftSide << shiftCount;
        }

        public static BigNumber operator >>(BigNumber leftSide, int shiftCount)
        {
            var da = new DigitsArray(leftSide.digits);
            da.DataUsed = da.ShiftRight(shiftCount);

            if (leftSide.IsNegative)
            {
                for (var i = da.Count - 1; i >= da.DataUsed; i--)
                {
                    da[i] = DigitsArray.AllBits;
                }

                var mask = DigitsArray.HiBitSet;
                for (var i = 0; i < DigitsArray.DataSizeBits; i++)
                {
                    if ((da[da.DataUsed - 1] & mask) == DigitsArray.HiBitSet)
                    {
                        break;
                    }

                    da[da.DataUsed - 1] |= mask;
                    mask >>= 1;
                }

                da.DataUsed = da.Count;
            }

            return new BigNumber(da);
        }

        private static BigNumber RightShift(BigNumber leftSide, int shiftCount)
        {
            return leftSide >> shiftCount;
        }

        #endregion------------------------------------------------------------\\

        #region -- relational operator --------------------

        /// <summary>
        /// Compare this instance to a specified object and returns indication of their relative value.
        /// </summary>
        /// <param name="value">An object to compare, or a null reference (<b>Nothing</b> in Visual Basic).</param>
        /// <returns>A signed number indicating the relative value of this instance and <i>value</i>.
        /// <list type="table">
        ///		<listheader>
        ///			<term>Return Value</term>
        ///			<description>Description</description>
        ///		</listheader>
        ///		<item>
        ///			<term>Less than zero</term>
        ///			<description>This instance is less than <i>value</i>.</description>
        ///		</item>
        ///		<item>
        ///			<term>Zero</term>
        ///			<description>This instance is equal to <i>value</i>.</description>
        ///		</item>
        ///		<item>
        ///			<term>Greater than zero</term>
        ///			<description>
        ///				This instance is greater than <i>value</i>. 
        ///				<para>-or-</para>
        ///				<i>value</i> is a null reference (<b>Nothing</b> in Visual Basic).
        ///			</description>
        ///		</item>
        /// </list>
        /// </returns>
        public int CompareTo(BigNumber value)
        {
            return Compare(this, value);
        }

        /// <summary>
        /// Compare two objects and return an indication of their relative value.
        /// </summary>
        /// <param name="leftSide">An object to compare, or a null reference (<b>Nothing</b> in Visual Basic).</param>
        /// <param name="rightSide">An object to compare, or a null reference (<b>Nothing</b> in Visual Basic).</param>
        /// <returns>A signed number indicating the relative value of this instance and <i>value</i>.
        /// <list type="table">
        ///		<listheader>
        ///			<term>Return Value</term>
        ///			<description>Description</description>
        ///		</listheader>
        ///		<item>
        ///			<term>Less than zero</term>
        ///			<description>This instance is less than <i>value</i>.</description>
        ///		</item>
        ///		<item>
        ///			<term>Zero</term>
        ///			<description>This instance is equal to <i>value</i>.</description>
        ///		</item>
        ///		<item>
        ///			<term>Greater than zero</term>
        ///			<description>
        ///				This instance is greater than <i>value</i>. 
        ///				<para>-or-</para>
        ///				<i>value</i> is a null reference (<b>Nothing</b> in Visual Basic).
        ///			</description>
        ///		</item>
        /// </list>
        /// </returns>
        public static int Compare(BigNumber leftSide, BigNumber rightSide)
        {
            if (leftSide.Equals(rightSide))
            {
                return 0;
            }

            if (leftSide > rightSide) return 1;
            if (leftSide == rightSide) return 0;
            return -1;
        }

        public static bool operator ==(BigNumber leftSide, BigNumber rightSide)
        {
            if (leftSide.Equals(rightSide))
            {
                return true;
            }

            return leftSide.IsNegative == rightSide.IsNegative && leftSide.Equals(rightSide);
        }

        public static bool operator !=(BigNumber leftSide, BigNumber rightSide)
        {
            return !(leftSide == rightSide);
        }

        public static bool operator >(BigNumber leftSide, BigNumber rightSide)
        {
            if (leftSide.IsNegative != rightSide.IsNegative)
            {
                return rightSide.IsNegative;
            }

            if (leftSide.digits.DataUsed != rightSide.digits.DataUsed)
            {
                return leftSide.digits.DataUsed > rightSide.digits.DataUsed;
            }

            for (var idx = leftSide.digits.DataUsed - 1; idx >= 0; idx--)
            {
                if (leftSide.digits[idx] != rightSide.digits[idx])
                {
                    return (leftSide.digits[idx] > rightSide.digits[idx]);
                }
            }

            return false;
        }

        public static bool operator <(BigNumber leftSide, BigNumber rightSide)
        {
            if (leftSide.IsNegative != rightSide.IsNegative)
            {
                return leftSide.IsNegative;
            }

            if (leftSide.digits.DataUsed != rightSide.digits.DataUsed)
            {
                return leftSide.digits.DataUsed < rightSide.digits.DataUsed;
            }

            for (var idx = leftSide.digits.DataUsed - 1; idx >= 0; idx--)
            {
                if (leftSide.digits[idx] != rightSide.digits[idx])
                {
                    return (leftSide.digits[idx] < rightSide.digits[idx]);
                }
            }

            return false;
        }

        public static bool operator >=(BigNumber leftSide, BigNumber rightSide)
        {
            return Compare(leftSide, rightSide) >= 0;
        }

        public static bool operator <=(BigNumber leftSide, BigNumber rightSide)
        {
            return Compare(leftSide, rightSide) <= 0;
        }

        #endregion------------------------------------------------------------\\

        #region -- object override --------------------

        /// <summary>
        /// Determines whether two BigNumber instances are equal.
        /// </summary>
        /// <param name="other">An <see cref="BigNumbers.BigNumber">Object</see> to compare with this instance.</param>
        /// <returns></returns>
        /// <seealso cref="BigNumbers.BigNumber">System.Object</seealso> 
        public bool Equals(BigNumber other)
        {
            if (digits.DataUsed != other.digits.DataUsed)
            {
                return false;
            }

            for (var idx = 0; idx < digits.DataUsed; idx++)
            {
                if (digits[idx] != other.digits[idx])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether two Object instances are equal.
        /// </summary>
        /// <param name="obj">An <see cref="System.Object">Object</see> to compare with this instance.</param>
        /// <returns></returns>
        /// <seealso cref="System.Object">System.Object</seealso> 
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null))
            {
                return false;
            }

            var c = (BigNumber) obj;
            if (digits.DataUsed != c.digits.DataUsed)
            {
                return false;
            }

            for (var idx = 0; idx < digits.DataUsed; idx++)
            {
                if (digits[idx] != c.digits[idx])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer has code.</returns>
        /// <seealso cref="System.Object">System.Object</seealso> 
        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return digits.GetHashCode();
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent base 10 string representation.
        /// </summary>
        /// <returns>A <see cref="System.String">String</see> in base 10.</returns>
        /// <seealso cref="System.Object">System.Object</seealso> 
        public override string ToString()
        {
            return ToString(10);
        }

        #endregion------------------------------------------------------------\\

        #region -- type conversion --------------------

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation in specified base.
        /// </summary>
        /// <param name="radix">Int radix between 2 and 36</param>
        /// <returns>A string.</returns>
        public string ToString(int radix)
        {
            if (radix < 2 || radix > 36)
            {
                throw new ArgumentOutOfRangeException(nameof(radix));
            }

            if (IsZero)
            {
                return "0";
            }

            BigNumber remainder;
            BigNumber biRadix = radix;

            const string charSet = "0123456789abcdefghijklmnopqrstuvwxyz";
            var al = new System.Collections.ArrayList();
            var quotient = this;
            while (!quotient.IsZero)
            {
                Divide(quotient, biRadix, out quotient, out remainder);
                al.Insert(0, charSet[(int) remainder.digits[0]]);
            }

            var result = new string((char[]) al.ToArray(typeof(char)));
            if (radix == 10 && IsNegative)
            {
                return "-" + result;
            }

            return result;
        }

        /// <summary>
        /// Returns string in hexidecimal of the internal digit representation.
        /// </summary>
        /// <remarks>
        /// This is not the same as ToString(16). This method does not return the sign, but instead
        /// dumps the digits array into a string representation in base 16.
        /// </remarks>
        /// <returns>A string in base 16.</returns>
        public string ToHexString()
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("{0:X}", digits[digits.DataUsed - 1]);

            var f = "{0:X" + (2 * DigitsArray.DataSizeOf) + "}";
            for (var i = digits.DataUsed - 2; i >= 0; i--)
            {
                sb.AppendFormat(f, digits[i]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns BigNumber as System.Int16 if possible.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Int value of BigNumber</returns>
        /// <exception cref="System.Exception">When BigNumber is too large to fit into System.Int16</exception>
        public static int ToInt16(BigNumber value)
        {
            return short.Parse(value.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns BigNumber as System.UInt16 if possible.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">When BigNumber is too large to fit into System.UInt16</exception>
        public static uint ToUInt16(BigNumber value)
        {
            return ushort.Parse(value.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns BigNumber as System.Int32 if possible.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">When BigNumber is too large to fit into System.Int32</exception>
        public static int ToInt32(BigNumber value)
        {
            return int.Parse(value.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns BigNumber as System.UInt32 if possible.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">When BigNumber is too large to fit into System.UInt32</exception>
        public static uint ToUInt32(BigNumber value)
        {
            return uint.Parse(value.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns BigNumber as System.Int64 if possible.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">When BigNumber is too large to fit into System.Int64</exception>
        public static long ToInt64(BigNumber value)
        {
            return long.Parse(value.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns BigNumber as System.UInt64 if possible.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">When BigNumber is too large to fit into System.UInt64</exception>
        public static ulong ToUInt64(BigNumber value)
        {
            return ulong.Parse(value.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        #endregion------------------------------------------------------------\\


        int IComparable<BigNumber>.CompareTo(BigNumber other)
        {
            return CompareTo(other);
        }

        public void Initialize(string number)
        {
            //todo
        }

        public IBigNumber Add(IBigNumber second)
        {
            return this + (BigNumber) second;
        }

        public IBigNumber Sub(IBigNumber second)
        {
            return this - (BigNumber) second;
        }

        public IBigNumber Mul(IBigNumber second)
        {
            return this * (BigNumber) second;
        }

        public IBigNumber Div(IBigNumber second)
        {
            return this / (BigNumber) second;
        }

        public IBigNumber Mod(IBigNumber second)
        {
            return this % (BigNumber) second;
        }

        public IBigNumber Clone()
        {
            return new BigNumber(this);
        }

        public IBigNumber Construct(string number)
        {
            return new BigNumber(number);
        }

        /// <summary>
        /// slower and more gc allocation than Equals because need cast interface to BigNumber
        /// </summary>
        /// <param name="second"></param>
        /// <returns></returns>
        public bool Equal(IBigNumber second)
        {
            return this == (BigNumber) second;
        }

        public bool Smaller(IBigNumber second)
        {
            return this < (BigNumber) second;
        }

        public static void Serialize(BigNumber number, System.IO.BinaryWriter writer)
        {
            if (!writer.BaseStream.CanWrite) throw new ArgumentException("The writer you provided is not writable");

            var count = number.digits.Count;
            if (number.IsNegative) count = -count;
            writer.Write(count);
            number.digits.Serialize(writer);
            using (var sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider()) //new SHA1CryptoServiceProvider() faster than using SHA1.Create()
            {
                var data = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(number.ToHexString()));
                writer.Write(data);
            }
        }

        public static BigNumber Deserialize(System.IO.BinaryReader reader)
        {
            if (!reader.BaseStream.CanRead) throw new ArgumentException("The reader you provided is not readable");
            try
            {
                var result = new BigNumber();
                var count = reader.ReadInt32();

                if (count < 0) result.negative = true;
                count = Math.Abs(count);
                result.digits.Deserialize(reader, count);
                var sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                var checkSum = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(result.ToHexString()));
                var data = reader.ReadBytes(checkSum.Length);
                for (var i = 0; i < checkSum.Length; i++)
                {
                    if (data[i] != checkSum[i]) throw new System.IO.IOException("Error deserializing BigNumber " + result.ToString());
                }

                return result;
            }
            catch (System.IO.EndOfStreamException)
            {
                throw new System.IO.IOException("Unable to deserialize a BigNumber from a file");
            }
        }
    }
}