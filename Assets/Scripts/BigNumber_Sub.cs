﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;

namespace UnityModule.BigNumbers
{
    public partial class BigNumber
    {
        public static void Sub(BigNumber source, BigNumber destination, BigNumber result)
        {
            if (Math.Abs(destination.readableValue) < EPSILON)
            {
                Copy(source, result);
                return;
            }

            if (Math.Abs(source.readableValue) < EPSILON)
            {
                Copy(destination, result);
                result.readableValue *= -1f;
                result.ConvertMaximum();
                return;
            }

            result.pow = source.pow;
            if (source.pow == destination.pow)
            {
                result.readableValue = source.readableValue - destination.readableValue;
            }
            else if (source.pow > destination.pow)
            {
                result.readableValue = source.readableValue - destination.readableValue * (float) Math.Pow(10, -(source.pow - destination.pow));
            }
            else if (source.pow < destination.pow)
            {
                result.readableValue = source.readableValue * (float) Math.Pow(10, -(destination.pow - source.pow)) - destination.readableValue;
                result.pow = destination.pow;
            }

            result.ConvertMaximum();
        }
    }
}