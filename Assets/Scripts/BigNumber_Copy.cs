﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

namespace UnityModule.BigNumbers
{
    public partial class BigNumber
    {
        /// <summary>
        /// copy source to destination
        /// </summary>
        /// <param name="source">BigNumber</param>
        /// <param name="destination">BigNumber</param>
        private static void Copy(BigNumber source, BigNumber destination)
        {
            destination.readableValue = source.readableValue;
            destination.pow = source.pow;
            destination.valueLength = source.valueLength;
        }
    }
}