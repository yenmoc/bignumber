﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;

namespace UnityModule.BigNumbers
{
    public static class BigNumberExtension
    {
        public static void ConvertMaximum(this BigNumber number)
        {
            var tempReadableVal = number.readableValue;
            if (number.readableValue < 0)
                tempReadableVal *= -1;

            while (tempReadableVal > BigNumber.MaxReadValue)
            {
                number.readableValue /= 10;
                tempReadableVal /= 10;
                number.pow++;
            }

            while (tempReadableVal < BigNumber.MaxReadValue / 10 && number.pow > 0)
            {
                number.readableValue *= 10;
                tempReadableVal *= 10;
                number.pow--;
            }

            if (number.pow < 0f)
            {
                number.readableValue *= (float) System.Math.Pow(10, number.pow);
                number.pow = 0;
            }

            if (Math.Abs(number.readableValue) < BigNumber.EPSILON)
                number.pow = 0;

            number.valueLength = GetLength(number.readableValue);
        }

        internal static int GetLength(double number)
        {
            if (number < 0)
                number *= -1;
            if (Math.Abs(number) < BigNumber.EPSILON)
                return 1;
            return (int) Math.Floor(Math.Log10(number)) + 1;
        }
    }
}