﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;

namespace UnityModule.BigNumbers
{
    public partial class BigNumber
    {
        private static void Mul(BigNumber source, BigNumber destination, BigNumber result)
        {
            if (Math.Abs(destination.readableValue) < EPSILON || Math.Abs(source.readableValue) < EPSILON)
            {
                result.Reset();
                return;
            }

            int addPow = 0;
            if (source.pow == destination.pow)
            {
                result.readableValue = source.readableValue * destination.readableValue;
            }
            else if (source.pow > destination.pow)
            {
                var stripValue = destination.readableValue / (float) Math.Pow(10, destination.valueLength - 1);
                addPow = destination.valueLength - 1;
                result.readableValue = source.readableValue * stripValue;
            }
            else if (source.pow < destination.pow)
            {
                var stripValue = source.readableValue / (float) Math.Pow(10, source.valueLength - 1);
                addPow = source.valueLength - 1;
                result.readableValue = destination.readableValue * stripValue;
            }

            result.pow = source.pow + addPow + destination.pow;

            result.ConvertMaximum();
        }
    }
}