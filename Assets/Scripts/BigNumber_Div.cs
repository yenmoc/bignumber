﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;

namespace UnityModule.BigNumbers
{
    public partial class BigNumber
    {
        private static void Div(BigNumber source, BigNumber destination, BigNumber result)
        {
            if (Math.Abs(source.readableValue) < EPSILON)
            {
                Copy(source, result);
                return;
            }

            if (Math.Abs(destination.readableValue) < EPSILON)
            {
                throw new DivideByZeroException();
            }

            var t = (float) Math.Pow(10, 5);
            if (source.readableValue > MaxReadValue / t)
            {
                source.readableValue /= t;
                source.pow += 5;
            }

            if (destination.readableValue > MaxReadValue / t)
            {
                destination.readableValue /= t;
                destination.pow += 5;
            }

            result.readableValue = source.readableValue / destination.readableValue;
            result.pow = source.pow - destination.pow;

            result.ConvertMaximum();
        }
    }
}