﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/



namespace UnityModule.BigNumbers
{
    public partial class BigNumber
    {
        private static void From(BigNumber number, string value)
        {
            number.Reset();
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            var part1 = value;
            var part2 = "";
            string[] parts;
            if (value.Contains("E"))
            {
                parts = part1.Split('E');
                part1 = parts[0];
                part2 = parts[1];
                int.TryParse(part2, out number.pow);
            }

            parts = part1.Split('.');
            var len = parts[0].Length;
            if (len <= MAX_READ_LENGTH)
            {
                number.readableValue = float.Parse(part1);
                number.pow += 0;
            }
            else
            {
                var readablePart = parts[0].Substring(0, MAX_READ_LENGTH);
                var remainLen = len - readablePart.Length;
                if (float.TryParse(readablePart, out number.readableValue))
                {
                    number.pow += remainLen;
                }
                else
                {
                    UnityEngine.Debug.LogError("Could not convert value " + parts[0]);
                }
            }

            number.ConvertMaximum();
        }

        private static void From(BigNumber number, int value, int pow = 0)
        {
            number.readableValue = value;
            number.pow = pow;
            number.ConvertMaximum();
        }

        private static void From(BigNumber number, float value, int pow = 0)
        {
            number.readableValue = value;
            number.pow = pow;
            number.ConvertMaximum();
        }

        private static void From(BigNumber number, double value, int pow = 0)
        {
            int length = BigNumberExtension.GetLength(value);
            if (length < MAX_READ_LENGTH)
            {
                number.readableValue = (float) value;
                number.pow = pow;
            }
            else
            {
                number.readableValue = (float) (value / System.Math.Pow(10, length - MAX_READ_LENGTH));
                number.pow = length - MAX_READ_LENGTH;
            }

            number.ConvertMaximum();
        }

        private static void From(BigNumber number, long value, int pow = 0)
        {
            number.readableValue = value;
            number.pow = pow;
            number.ConvertMaximum();
        }

        private static void From(BigNumber number, float value, string unit)
        {
            number.readableValue = value;
            number.pow = GetPowFromUnit(unit.ToUpper());
            number.ConvertMaximum();
        }
        
    }
}