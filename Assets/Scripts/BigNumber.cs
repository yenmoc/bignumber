﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;
using System.Text;

namespace UnityModule.BigNumbers
{
    public partial class BigNumber : IComparable<BigNumber>
    {
        private const int MAX_READ_LENGTH = 12; //max length of float is 38 but we dont care any number after this position
        public static readonly float MaxReadValue = float.Parse("999999999999"); //increase length of this nuber will increse the acurration
        public const float EPSILON = 1.0E-10f;
        private readonly StringBuilder _numberBuilder = new StringBuilder();
        private readonly StringBuilder _powBuilder = new StringBuilder();

        #region -- main property --------------------

        public float readableValue;
        public int pow;
        public int valueLength;

        #endregion------------------------------------------------------------\\

        #region constructor and operator

        /// <summary>
        /// reset bignumber (readableValue = 0, pow = 0, valueLength = 1)
        /// </summary>
        public void Reset()
        {
            readableValue = 0;
            pow = 0;
            valueLength = 1;
        }

        public BigNumber()
        {
            Reset();
        }

        public BigNumber(BigNumber source)
        {
            Copy(source, this);
        }

        public static implicit operator BigNumber(long value)
        {
            var number = new BigNumber();
            From(number, value);
            return number;
        }

        public static implicit operator BigNumber(float value)
        {
            var number = new BigNumber();
            From(number, $"{value}");
            return number;
        }

        public static implicit operator BigNumber(double value)
        {
            var number = new BigNumber();
            From(number, value);
            return number;
        }

        public static implicit operator BigNumber(string value)
        {
            var number = new BigNumber();
            From(number, value);
            return number;
        }

        public static BigNumber operator +(BigNumber a, BigNumber b)
        {
            var c = new BigNumber();
            Add(a, b, c);
            return c;
        }

        public static BigNumber operator +(BigNumber a, long b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Add(a, c, d);
            return d;
        }

        public static BigNumber operator +(BigNumber a, float b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Add(a, c, d);
            return d;
        }

        public static BigNumber operator +(BigNumber a, int b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Add(a, c, d);
            return d;
        }

        public static BigNumber operator -(BigNumber a, BigNumber b)
        {
            var c = new BigNumber();
            Sub(a, b, c);
            return c;
        }

        public static BigNumber operator -(BigNumber a, long b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Sub(a, c, d);
            return d;
        }

        public static BigNumber operator -(BigNumber a, float b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Sub(a, c, d);
            return d;
        }

        public static BigNumber operator -(BigNumber a, int b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Sub(a, c, d);
            return d;
        }

        public static BigNumber operator *(BigNumber a, BigNumber b)
        {
            var c = new BigNumber();
            Mul(a, b, c);
            return c;
        }

        public static BigNumber operator *(BigNumber a, long b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Mul(a, c, d);
            return d;
        }

        public static BigNumber operator *(BigNumber a, float b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Mul(a, c, d);
            return d;
        }

        public static BigNumber operator *(BigNumber a, int b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Mul(a, c, d);
            return d;
        }

        public static BigNumber operator /(BigNumber a, BigNumber b)
        {
            var c = new BigNumber();
            Div(a, b, c);
            return c;
        }

        public static BigNumber operator /(BigNumber a, long b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Div(a, c, d);
            return d;
        }

        public static BigNumber operator /(BigNumber a, float b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Div(a, c, d);
            return d;
        }

        public static BigNumber operator /(BigNumber a, int b)
        {
            var d = new BigNumber();
            BigNumber c = b;
            Div(a, c, d);
            return d;
        }

        #endregion

        public bool HasValue()
        {
            return readableValue > 0;
        }

        public int Length()
        {
            return valueLength + pow;
        }

        public int ToInt()
        {
            if (pow <= 0)
            {
                if (readableValue <= int.MaxValue)
                    return (int) readableValue;

                UnityEngine.Debug.LogError("Value is too big that can not be Integer!");
                return 0;
            }

            var valueStr = GetString();
            if (int.TryParse(valueStr, out var outValue))
            {
                return outValue;
            }

            UnityEngine.Debug.LogError("Value is invalid, can not be Integer! " + valueStr);
            return int.MaxValue;
        }

        public long ToLong()
        {
            if (pow <= 0)
            {
                if (readableValue <= long.MaxValue)
                    return (long) readableValue;
                UnityEngine.Debug.LogError("Value is too big that can not be Integer!");
                return 0;
            }

            var valueStr = GetString();
            if (long.TryParse(valueStr, out var outValue))
            {
                return outValue;
            }

            UnityEngine.Debug.LogError("Value is invalid, can not be Integer! " + valueStr);
            return long.MaxValue;
        }

        public string GetString(bool stripDecimal = true)
        {
            var len = valueLength + pow;
            if (len <= 3)
            {
                if (stripDecimal)
                {
                    var numberStr = Math.Round(readableValue, 0).ToString("#");
                    if (string.IsNullOrEmpty(numberStr))
                        numberStr = "0";
                    return numberStr;
                }

                return $"{Math.Round(readableValue, 2)}";
            }

            if (pow == 0)
            {
                if (Math.Abs(readableValue) < EPSILON)
                    return "0";

                if (stripDecimal)
                {
                    var numberStr = readableValue.ToString("#");
                    if (string.IsNullOrEmpty(numberStr))
                        numberStr = "0";
                    return numberStr;
                }
                else
                {
                    string numberStr;
                    if ($"{readableValue}".Contains("E"))
                    {
                        numberStr = readableValue.ToString("#");
                        if (string.IsNullOrEmpty(numberStr))
                            numberStr = "0";
                    }
                    else
                        numberStr = $"{readableValue}";

                    return numberStr;
                }
            }
            else
            {
                string numberStr;
                if ($"{readableValue}".Contains("E"))
                {
                    numberStr = readableValue.ToString("#");
                    if (string.IsNullOrEmpty(numberStr))
                        numberStr = "0";
                }
                else
                    numberStr = $"{readableValue}";

                var parts = numberStr.Split('.');
                if (parts.Length == 2)
                {
                    var p2Len = parts[1].Length;
                    if (p2Len > pow)
                    {
                        _powBuilder.Clear().Append(parts[1].Substring(0, pow));
                    }
                    else if (p2Len == pow)
                    {
                        _powBuilder.Clear().Append(parts[1]);
                    }
                    else if (p2Len < pow)
                    {
                        _powBuilder.Clear().Append(parts[1]);
                        var a = pow - p2Len;
                        while (a > 0)
                        {
                            _powBuilder.Append("0");
                            a--;
                        }
                    }

                    return _numberBuilder.Clear().Append(parts[0]).Append(_powBuilder).ToString();
                }

                if (parts.Length == 1)
                {
                    _powBuilder.Clear();
                    for (var i = 0; i < pow; i++)
                        _powBuilder.Append("0");

                    return _numberBuilder.Clear().Append(parts[0]).Append(_powBuilder).ToString();
                }

                UnityEngine.Debug.LogError("Number should never have more than one dot!");
                return "0";
            }
        }

        public string GetNotationString()
        {
            if (pow > 0)
            {
                var len = valueLength;
                var num = readableValue / (float) Math.Pow(10, len - 1);
                num = (float) Math.Round(num, 2);
                _numberBuilder.Clear()
                    .Append(num)
                    .Append("E+")
                    .Append(pow + len - 1);
            }
            else
            {
                var len = valueLength;
                if (len > 3)
                {
                    var num = readableValue / (float) Math.Pow(10, len - 3);
                    num = (float) Math.Round(num, 2);
                    _numberBuilder.Clear();
                    _numberBuilder.Append(num);
                    if (pow + len - 3 >= 1)
                    {
                        _numberBuilder.Append("E+").Append(pow + len - 3);
                    }
                }
                else
                {
                    return $"{Math.Round(readableValue, 2)}";
                }
            }

            return _numberBuilder.ToString();
        }

        public string GetAlphabetString()
        {
            var len = valueLength + pow;
            if (len <= 3)
            {
                return $"{Math.Round(readableValue, 0)}";
            }

            var integerPart = (len - 1) % 3 + 1;
            var displayPart = readableValue / (float) Math.Pow(10, valueLength - integerPart);

            var truncate = (float) Math.Truncate(displayPart);
            if (displayPart - truncate > 0)
            {
                if (truncate >= 100)
                    displayPart = (float) Math.Round(displayPart);
                else if (truncate >= 10)
                    displayPart = (float) Math.Round(displayPart, 1);
                else
                    displayPart = (float) Math.Round(displayPart, 2);
            }
            else
                displayPart = truncate;

            _numberBuilder.Clear().Append(displayPart);

            if (len > 15)
            {
                var unitSize = (len - 16) / (3 * 26) + 2;
                var unitTypeInt = ((len - 16) / 3) % 26;
                var unitChar = (char) (65 + unitTypeInt);
                for (var i = 0; i < unitSize; i++)
                    _numberBuilder.Append(unitChar);
            }
            else if (len > 12)
                _numberBuilder.Append("T");
            else if (len > 9)
                _numberBuilder.Append("B");
            else if (len > 6)
                _numberBuilder.Append("M");
            else if (len > 3)
                _numberBuilder.Append("K");

            return _numberBuilder.ToString();
        }

        public string GetAlphabetUnit()
        {
            var len = valueLength + pow;
            if (len <= 3)
            {
                return "";
            }

            _numberBuilder.Clear();

            if (len > 15)
            {
                var unitSize = (len - 16) / (3 * 26) + 2;
                var unitTypeInt = ((len - 16) / 3) % 26;
                var unitChar = (char) (65 + unitTypeInt);
                for (var i = 0; i < unitSize; i++)
                    _numberBuilder.Append(unitChar);
            }
            else if (len > 12)
                _numberBuilder.Append("T");
            else if (len > 9)
                _numberBuilder.Append("B");
            else if (len > 6)
                _numberBuilder.Append("M");
            else if (len > 3)
                _numberBuilder.Append("K");

            return _numberBuilder.ToString();
        }

        public override string ToString()
        {
            return GetString();
        }

        public bool GreaterThan(BigNumber number)
        {
            if (pow == number.pow)
                return readableValue > number.readableValue;
            return pow > number.pow;
        }

        public static int GetPowFromUnit(string unit)
        {
            var pow = 0;
            if (unit.Length == 1)
            {
                switch (unit)
                {
                    case "K":
                        pow = 3;
                        break;
                    case "M":
                        pow = 6;
                        break;
                    case "B":
                        pow = 9;
                        break;
                    case "T":
                        pow = 12;
                        break;
                }
            }
            else if (unit.Length > 1)
            {
                var unitSize = unit.Length;
                var unitTypeInt = unit[0] - 65;
                var length = (unitSize - 2) * (3 * 26) + 15 + (unitTypeInt * 3);
                pow = length - 1;
            }

            return pow;
        }

        public int CompareTo(BigNumber other)
        {
            if (pow == other.pow)
            {
                if (readableValue > other.readableValue)
                    return 1;
                if (Math.Abs(readableValue - other.readableValue) < EPSILON)
                    return 0;
                return -1;
            }

            if (pow > other.pow)
                return 1;
            if (pow < other.pow)
                return -1;

            return 0;
        }

        public int CompareTo(float number)
        {
            if (Math.Abs(number) < EPSILON)
                return readableValue.CompareTo(number);

            return readableValue > number ? 1 : CompareTo(new BigNumber(number));
        }
    }
}