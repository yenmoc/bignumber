# BigNumber

## Installation

```bash
"com.yenmoc.bignumber":"https://gitlab.com/yenmoc/bignumber"
or
npm publish --registry http://localhost:4873
```

## Use

```csharp
        BigNumber a = "10000000000000";
        BigNumber b = "4500000000000";
        Debug.Log("A =" + a.GetAlphabetString());
        Debug.Log("B =" + b.GetAlphabetString());
        var c = a + b;
        Debug.Log("result =" + c.GetAlphabetString());
```

